# Twitch Alerts API Client for GO

[![Build Status](https://drone.io/bitbucket.org/muxy/twitchalerts/status.png)](https://drone.io/bitbucket.org/muxy/twitchalerts/latest)
[![MIT](http://img.shields.io/badge/license-MIT-green.svg)](LICENSE)