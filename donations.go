package twitchalerts

import (
	"fmt"
	"net/url"
)

type DonationsResponse struct {
	Donations []Donation `json:"data"`
}

type Donation struct {
	DonationID int64     `json:"donation_id"`
	CreatedAt  Timestamp `json:"created_at"`
	Currency   string    `json:"currency"`
	Amount     float64   `json:"amount,string"`
	Name       string    `json:"name"`
	Message    string    `json:"message"`
}

func (client *TwitchAlertsClient) GetDonations() (*DonationsResponse, error) {
	var donations DonationsResponse
	var e TwitchAlertsError

	v := &url.Values{}
	v.Add("limit", "100")

	resp, err := client.session.Get("https://streamlabs.com/api/v1.0/donations", v, &donations, &e)
	if err != nil {
		return nil, err
	}

	if resp.Status() != 200 {
		return nil, fmt.Errorf("Status: %d, Error: %s, Message: %s", resp.Status(), e.Error, e.Message)
	}

	return &donations, nil
}

func (client *TwitchAlertsClient) GetDonationsBeforeID(donationID int64) (*DonationsResponse, error) {
	var donations DonationsResponse
	var e TwitchAlertsError

	v := &url.Values{}
	v.Add("limit", "100")
	v.Add("before", fmt.Sprintf("%d", donationID))

	resp, err := client.session.Get("https://streamlabs.com/api/v1.0/donations", v, &donations, &e)
	if err != nil {
		return nil, err
	}

	if resp.Status() != 200 {
		return nil, fmt.Errorf("Status: %d, Error: %s, Message: %s", resp.Status(), e.Error, e.Message)
	}

	return &donations, nil
}
