package twitchalerts_test

import (
	"encoding/json"
	"reflect"
	"testing"
	"time"

	"bitbucket.org/muxy/twitchalerts"
)

func TestDecodeExample(t *testing.T) {

	exampleData := `
{
 "data":[
    {
      "donation_id":80179029,
      "created_at":"1438576556",
      "currency":"USD",
      "amount":"50",
      "name":"Thomas",
      "message":"nice!"
    },
    {
      "donation_id":80179019,
      "created_at":"1438576521",
      "currency":"USD",
      "amount":"15.50",
      "name":"JoeGamer",
      "message":null
    }
  ]
}
`

	var response twitchalerts.DonationsResponse
	err := json.Unmarshal([]byte(exampleData), &response)
	if err != nil {
		t.Error(err)
	}

	if len(response.Donations) != 2 {
		t.Error("Donations list was the wrong length")
	}

	donation := twitchalerts.Donation{
		DonationID: 80179029,
		CreatedAt: twitchalerts.Timestamp{
			Time: time.Unix(1438576556, 0),
		},
		Currency: "USD",
		Amount:   50,
		Name:     "Thomas",
		Message:  "nice!",
	}

	if !reflect.DeepEqual(response.Donations[0], donation) {
		t.Errorf("Donation objects not equal, expected: %v got: %v", donation, response.Donations[0])
	}

	donation = twitchalerts.Donation{
		DonationID: 80179019,
		CreatedAt: twitchalerts.Timestamp{
			Time: time.Unix(1438576521, 0),
		},
		Currency: "USD",
		Amount:   15.50,
		Name:     "JoeGamer",
		Message:  "",
	}

	if !reflect.DeepEqual(response.Donations[1], donation) {
		t.Errorf("Donation objects not equal, expected: %v got: %v", donation, response.Donations[1])
	}
}
