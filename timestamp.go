package twitchalerts

// https://gist.github.com/bsphere/8369aca6dde3e7b4392c

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

type Timestamp struct {
	time.Time
}

func (t *Timestamp) MarshalJSON() ([]byte, error) {
	ts := t.Time.Unix()
	stamp := fmt.Sprint(ts)

	return []byte(stamp), nil
}

func (t *Timestamp) UnmarshalJSON(b []byte) error {
	data := strings.Replace(string(b), "\"", "", -1)
	ts, err := strconv.Atoi(data)
	if err != nil {
		return err
	}

	t.Time = time.Unix(int64(ts), 0)

	return nil
}
