package twitchalerts

import (
	"net/url"

	"gopkg.in/jmcvetta/napping.v3"
)

type TwitchAlertsClient struct {
	session napping.Session
}

func NewTwitchAlertsClient(accessToken string) TwitchAlertsClient {

	v := &url.Values{}
	v.Add("access_token", accessToken)
	return TwitchAlertsClient{
		session: napping.Session{
			Params: v,
		},
	}
}

type TwitchAlertsError struct {
	Error   string `json:"error"`
	Message string `json:"Message"`
}
